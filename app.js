function distance(first, second){
	//TODO: implementați funcția

	// arunc InvalidType dacă unul dintre parametri nu este un array	
	if(!Array.isArray(first) || !Array.isArray(second)) { //verific dacă sunt array-uri
		throw new TypeError("InvalidType")
	   }
	
	// compar 2 array-uri vide si returnez 0
	if(first.length==0 || second.length==0) {
			return 0;
		}
		
	// elimin elementele duplicate
	 const Var1= new Set(first)
	 first= Array.from(Var1)
	 
	 const Var2= new Set(second)
	 second= Array.from(Var2)

	
	var c=0;
	for(var i=0; i<first.length; i++)
	for(var j=0; j<second.length; j++) {
		if(first[i] != second[j]) 
		  {
			  c++;
		  }
		if(first[i] !== second[j] && first[i] == second[j])
		  c+=2;
	}
	
	return c;

	
}


module.exports.distance = distance